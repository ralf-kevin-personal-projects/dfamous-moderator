<?php include '../headers/dashboard-header.php'; ?>
            
      
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Announcement</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
            <button class="btn btn-sm btn-outline-secondary">Export</button> -->
            <!-- <button class="btn btn-md btn-outline-secondary">Add New Job Post</button> -->
            
        </div>
        <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
            This week
        </button> -->
        </div>
    </div>
    
    <div class="">

            <div class="table-responsive">
                <table class="table table-striped table-sm">
                <thead>
                    <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Date Created</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>Hiring Software Engineer</td>
                    <td>At least 3yrs of experience</td>
                    <td>2018-18-20</td>
                    <td>
                        Published
                    </td>
                    <td>
                        <div class="form-group">
                            <button class="btn btn-sm btn-success" onclick=approve()>
                                <i class="fas fa-check"></i>
                            </button>
                            <button class="btn btn-sm btn-danger" onclick=archive()>
                                <i class="fas fa-times"></i>
                            </button>                            
                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#annModal">
                                <i class="fas fa-eye"></i>
                            </button>
                        </div>
                    </td>
                    </tr>                              
                </tbody>
                </table>
            </div>
    </div>


    </main>



<!-- Modal -->
<div class="modal fade" id="annModal" tabindex="-1" role="dialog" aria-labelledby="annModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="annModalTitle">Announcement</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">
            
                <h6>Announcement</h6>
                <div class="row">              
                    <div class="col-md-12">
                        <label>Announcement Title</label>                                                                
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Announcement Title"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                            <label>Description</label>                                                                
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Description" rows=5></textarea>
                            </div>
                        </div>
                </div>

            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
      </div>

    </div>
  </div>
</div>

<?php include '../headers/dashboard-footer.php'; ?>
<script>
    function approve() {
        alert("approve")
    }    
    function view() {
        alert("View")
    }
    function archive() {
        alert("archive")
    }    
</script>